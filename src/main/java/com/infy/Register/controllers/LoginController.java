package com.infy.Register.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.infy.Register.model.UserModel;
import com.infy.Register.service.LoginService;


@RefreshScope
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RibbonClient(name="custribbon")


public class LoginController {

	@Autowired
	LoginService loginService;
	
	@Autowired
	RestTemplate restTemplate;

	
	@PostMapping(value = "/login")
	public Map<String, String> sanctionLoan(@RequestBody UserModel user) throws Exception {
		HashMap<String, String> map = new HashMap<>();
		Integer result = loginService.AuthenticateUser(user);
	    map.put("Success", result.toString());
	    return map;

	}
	
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	@ResponseBody
	public String getFoosBySimplePath() {
	    return "Get some Foos";
	}

	
}