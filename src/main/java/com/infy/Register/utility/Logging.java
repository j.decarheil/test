package com.infy.Register.utility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Before;



public class Logging {

	private Logger logger = LogManager.getLogger(this.getClass());

	@Before("execution(* com.infy.Register.service.*(..))")
	public void beforeAdvice() throws Exception {

	}
	
	@After("execution(* com.infy.Register.service.*(..))")
	public void after() throws Exception{

	}
	
	@AfterReturning(value = "execution(* com.infy.Register.service.*(..))")
	public void afterReturning(){

	}
	
	@AfterThrowing(pointcut = "execution(* com.infy.Register.service.*(..))", throwing = "exception")
	public void logServiceException(Exception exception) throws Exception {
		logger.error(exception.getMessage(), exception);
	}
}
