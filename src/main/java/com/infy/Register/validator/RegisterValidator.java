package com.infy.Register.validator;

import com.infy.Register.model.UserModel;


public class RegisterValidator {

	public static void validate(UserModel user) throws Exception{
		validateUsername(user.getUsername());
		validatePassword(user.getPassword());
	}

	public static Boolean validateUsername(String username) throws Exception {
		System.out.println(username);
		if(username.length() < 4) {		
			throw new Exception("Service.BAD_USERNAME");
		}else {
			return true;
		}
	}
	
	public static Boolean validatePassword(String password) throws Exception{
		if(password.length() < 4) {		
			throw new Exception("Service.BAD_PASSWORD");
		}else {
			return true;
		}
	}

}
