package com.infy.Register.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.infy.Register.entity.UserEntity;
import com.infy.Register.model.UserModel;

@Repository(value="registerDAO")
public class UserDAO {
	
	@PersistenceContext
	EntityManager entityManager;	
	
	public Integer AuthenticateUser(UserModel user)
	{
		List<UserEntity> userEntity = entityManager.createQuery("SELECT u from UserEntity u WHERE u.username = :username", UserEntity.class).setParameter("username", user.getUsername().toLowerCase()).getResultList();
		if(userEntity.isEmpty()) {

			return 0;
		}else
		{
			return 1;
		}
	}
	

}
