package com.infy.Register.dao;

import com.infy.Register.model.UserModel;

public interface UserDAOInterface {
	public Integer AuthenticateUser(UserModel user);
}
