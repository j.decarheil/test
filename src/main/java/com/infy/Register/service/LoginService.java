package com.infy.Register.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infy.Register.dao.UserDAO;
import com.infy.Register.model.UserModel;
import com.infy.Register.validator.RegisterValidator;

@Service(value="loginService")
public class LoginService {

	
	@Autowired
	UserDAO userDAO;
	
	public Integer AuthenticateUser(UserModel user) throws Exception
	{
		RegisterValidator.validate(user);
		Integer result = userDAO.AuthenticateUser(user);
		
		if(result == 0) {
			return 0;
		}else if(result == 1) {
			return 1;
		}else {
			return 2;
		}
	}
}
